namespace :pgq do
  desc "Generate PgQ config from database.yml"
  task :generate_config => :environment do
    require 'yaml'
    raise "RAILS_ROOT should be" unless Rails.root 
    raise "RAILS_ENV should be" unless Rails.env 
    
    cfg = if ENV['DATABASE_YML']
      YAML.load_file(ENV['DATABASE_YML']) if File.exists?(ENV['DATABASE_YML'])
    else
      file = Rails.root.join('config', 'database.yml').to_s
      YAML.load_file(file) if File.exists?(file)
    end
    
    raise "Not found database.yml" unless cfg
    cfg = cfg[ Rails.env ]
  
    pgq_config = <<-SQL
[pgqadm]
job_name = #{cfg['database']}_pgqadm
db = host=#{cfg['host'] || '127.0.0.1'} dbname=#{cfg['database']} user=#{cfg['username']} password=#{cfg['password']} port=#{cfg['port'] || 5432}

# how often to run maintenance [seconds]
maint_delay = 500

# how often to check for activity [seconds]
loop_delay = 0.05

logfile = log/%(job_name)s.log
pidfile = tmp/%(job_name)s.pid
    SQL
    
    output = ENV["PGQ_CONFIG"] || Rails.root.join('config',"pgq_#{Rails.env}.ini").to_s
    File.open(output, 'w'){|f| f.write(pgq_config) }
    puts "Config #{Rails.env} generated to '#{output}'"
  end

  desc "Install PgQ from file to database RAILS_ENV"
  task :install do
    conf = Rails.root.join('config',"pgq_#{Rails.env}.ini").to_s 

    puts "No file specified" and return unless File.exists?(conf) 

    puts "installing pgq, running: pgqadm.py #{conf} install"

    output = `which pgqadm.py && pgqadm.py #{conf} install 2>&1 || which pgqadm && pgqadm #{conf} install 2>&1`
    puts output
    if output =~ /pgq is installed/ || output =~ /Reading from.*?pgq.sql$/
      puts "PgQ installed successfully"
    else
      raise "Something went wrong(see above)... Check that you install skytools package and create #{conf}"
    end
  end

  desc 'Running worker'
  task :worker => :environment do
    queues = ENV['QUEUES']
    log_file = Rails.root.join('log','pgq_queues.log').to_s
    logger = Logger.new(log_file)
    w = Pgq::Worker.new(:logger => logger, :queues => queues)
    w.run
  end
end
