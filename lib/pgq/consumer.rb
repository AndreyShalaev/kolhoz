class Pgq::Consumer

  attr_accessor :logger, :queue, :consumer

  def initialize(logger, queue, consumer)
    self.logger = logger
    self.queue = queue
    self.consumer = consumer
  end

  def perform_batch

    batch_id = ActiveRecord::Base.next_batch(queue, consumer)
    return 0 unless batch_id
    pgq_events = ActiveRecord::Base.get_batch_events(batch_id)

    pgq_events.each do |event|
      begin
        # Todo: add handler exceptions (event failed)
        perform(event['ev_id'], event['ev_type'], event['ev_data'].present? ? Marshal.load(event['ev_data']) : nil)
      rescue
        logger.info 'Fffuuuuu'
      end
    end

    ActiveRecord::Base.finish_batch(batch_id)
    return pgq_events.nil? ? 0 : pgq_events.count
  end

  def perform(event_id, event_type, *args)
    raise 'release me please'
  end
end
