module Pgq::Api
  def create_queue(name)
    connection.select_value(sanitize_sql_array ['SELECT pgq.create_queue(?)', name])
  end

  def remove_queue(name)
    connection.select_value(sanitize_sql_array ['SELECT pgq.drop_queue(?)', name])
  end

  def register_consumer(name, consumer)
    connection.select_value(sanitize_sql_array ['SELECT pgq.register_consumer(?, ?)', name, consumer])
  end

  def unregister_consumer(name, consumer)
    connection.select_value(sanitize_sql_array ['SELECT pgq.unregister_cosumer(?, ?)', name, consumer])
  end

  def insert_event(queue, event_type, event_data)
    connection.select_value(sanitize_sql_array ['SELECT pgq.insert_event(?, ?, ?, ?, ?, ?, ?)',
                            queue, event_type, event_data, nil, nil, nil, nil])
  end

  def next_batch(queue, consumer)
    result = connection.select_value(sanitize_sql_array ['SELECT pgq.next_batch(?, ?)', queue, consumer])
    result ? result.to_i : nil
  end

  def get_batch_events(batch_id)
    connection.select_all(sanitize_sql_array ['SELECT * FROM pgq.get_batch_events(?)', batch_id])
  end

  def finish_batch(batch_id)
    connection.select_all(sanitize_sql_array ['SELECT pgq.finish_batch(?)', batch_id])
  end
end
