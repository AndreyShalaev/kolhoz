class Pgq::Producer
  class << self
    def enqueue(event_type, *args)
      ActiveRecord::Base.insert_event('default', event_type.to_s, Marshal.dump(args))
    end
  end
end
