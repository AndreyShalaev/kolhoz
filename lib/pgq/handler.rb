class Pgq::Handler
  class << self
    def database
      ActiveRecord::Base
    end
    
    def add_queue(name, consumer='default')
      database.create_queue(name)
      database.register_consumer(name, consumer)
    end

    def remove_queue(name, consumer='default')
      database.remove_queue(name)
      database.unregister_consumer(name, consumer)
    end
  end

  
end
