class Pgq::Worker

  attr_reader :logger, :consumers, :queues, :sleep_time

  def initialize(h)
    @logger = h[:logger] || Rails.logger
    @consumers = []

    @queues = h[:queues].split(',')

    @queues.each do |q|
      klass = "pgq_#{q}".camelize.constantize
      @consumers << klass.new(@logger, q.to_s, q.to_s)
    end
    @sleep_time = 0.5
  end

  def run
    # deamonization
    exit if fork
    Process.setsid
    exit if fork

    $0 = 'Super worker'

    Dir.chdir '/'
    STDIN.reopen '/dev/null'
    STDOUT.reopen '/dev/null', 'a'
    STDERR.reopen '/dev/null', 'a'
    # Reconnect in forked process
    ActiveRecord::Base.establish_connection

    logger.info "[#{Process.ppid} - #{Process.pid}] Worker runned for #{@queues.join(', ')} queues"

    loop do
      processed = process_batch
      sleep(@sleep_time) if processed == 0
    end
  end

  def process_batch
    counter = 0
    @consumers.each do |consumer|
      counter += consumer.perform_batch
    end
    counter
  end
end
