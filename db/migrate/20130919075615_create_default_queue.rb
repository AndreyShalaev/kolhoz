class CreateDefaultQueue < ActiveRecord::Migration
  def self.up
    Pgq::Handler.add_queue 'default'
  end

  def self.down
    Pgq::Handler.remove_queue 'default'
  end
end
