class PgqDefault < Pgq::Consumer
  def perform(event_id, event_type, *args)
    logger.info "Added two numbers. Result: #{args[0][0] + args[0][1]}"
  end
end
